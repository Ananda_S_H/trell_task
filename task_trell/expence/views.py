# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.shortcuts import render

# Create your views here.
# -*- coding: utf-8 -*-
# from __future__ import unicode_literals

from django.db.models import Sum
from django.shortcuts import render

# Create your views here.
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Expence


@api_view(['POST'])
def get_data(request):
    data = request.data['name']
    start_date = datetime.datetime.now().date() - datetime.timedelta(30)
    details = Expence.objects.filter(user=data, month__gte=start_date).values('category').annotate(totol_amount=Sum('expences'))
    total = 0
    for i in details:
        total = i['totol_amount'] + total
    return Response({"details":details,'total':total})