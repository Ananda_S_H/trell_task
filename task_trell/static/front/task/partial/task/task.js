angular.module('task').controller('TaskCtrl', function($scope, task) {

    $scope.verify = function(name) {
        console.log('verify', name);
        task.get_data(name).then(function(data) {
            $scope.details = data['details'];
            $scope.total = data['total']
            $scope.insertData = []
            for (var i = 0; i < $scope.details.length; i++) {
                var cat = $scope.details[i]['category']
                var exe = $scope.details[i]['totol_amount']
                var object = {
                    "category": cat,
                    "expences": exe
                }
                $scope.insertData.push(object);
            }
            console.log('details after for loop', $scope.insertData);

            var chart = AmCharts.makeChart("chartdiv", {
                "type": "pie",
                "theme": "light",
                "dataProvider": $scope.insertData,
                "valueField": "expences",
                "titleField": "category",
            });
        })
    }
});
